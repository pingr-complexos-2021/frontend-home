export default {
  state: () => ({
    tags: [],
    isLoaded: false,
  }),
  mutations: {
    updateTags(state, tags) {
      state.tags = tags
    },
    setLoaded(state) {
      state.isLoaded = true
    },
  },
  actions: {
    async fetchTags({ commit }) {
      try {
        const { tags } = await this.$tagnowApi.$get('/tagnow')
        commit('updateTags', tags)
        commit('setLoaded')
      } catch (error) {
        console.error(error)
      }
    },
  },
}