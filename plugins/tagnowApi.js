export default function ({ $axios, env }, inject) {
  // Create a custom axios instance
  const api = $axios.create()

  // Set baseURL to something different
  api.setBaseURL(env.tagnowBaseUrl)
  // Inject to context as $api
  inject('tagnowApi', api)
}
