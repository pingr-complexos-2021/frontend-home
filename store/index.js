export const state = () => ({
    email: '',
    password: '',
    username: '',
    token: '',
})

export const actions = {
    async signUp(payload) {
        const response = await this.$axios.post('/register', payload.state)
        this.commit('updateToken', response.data.accessToken)
        const jwtSub = JSON.parse(window.atob(response.data.accessToken.split('.')[1])).sub
        this.$router.push(`/users/${jwtSub}`);
    },

    async signIn(payload) {
        const response = await this.$axios.post('/register', payload.state)
        this.commit('updateToken', response.data.accessToken)
        const jwtSub = JSON.parse(window.atob(response.data.accessToken.split('.')[1])).sub
        this.$router.push(`/users/${jwtSub}`);
    }
}

export const mutations = { 
    updateEmail(state, payload) {
        state.email = payload
    },
    updatePassword(state, payload) {
        state.password = payload
    },
    updateUsername(state, payload) { 
        state.username = payload
    },
    updateToken(state, payload) {
        state.token = payload
    }
}